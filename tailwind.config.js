module.exports = {
	purge: ["public/index.html", "src/**/*.svelte"],
	theme: {
		extend: {
			colors: {
				francilin: {
					blue: "#0d89d4",
					orange: "#f47415",
				},
			},
			screens: {
				print: { raw: "print" },
			},
		},
		customForms: (theme) => ({
			default: {
				input: {
					borderColor: theme("colors.gray.600"),
					borderWidth: theme("borderWidth.2"),
				},
			},
		}),
	},
	variants: {},
	plugins: [
		require("@tailwindcss/custom-forms"), //
		require("tailwindcss-rtl"),
	],
}
