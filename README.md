# Francil'IN Carto

## Install

The application was developed using Node v14. You can install it using [nvm](https://github.com/nvm-sh/nvm) for example.

Install the dependencies...

```bash
cd francilin-carto
npm install
cp .env.example .env # adapt values in .env if needed
```

...then start [Rollup](https://rollupjs.org):

```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running. Edit a component file in `src`, save it, and reload the page to see your changes.

By default, the server will only respond to requests from localhost. To allow connections from other computers, edit the `sirv` commands in package.json to include the option `--host 0.0.0.0`.

## Building and running in production mode

Deploy to production is done by a GitLab CI pipeline, cf [`.gitlab-ci.yml`](./.gitlab-ci.yml).

Commits to `master` are deployed to https://francilin-carto.netlify.app/

## Updating i18n messages

Source message file is `src/messages/messages.csv`

To make your changes effective, call (in `src/messages` folder):

```
./msg2json.py
```

`src/messages/fr.json` and `src/messages/en.json` will be re-generated.

## Updating services

Services are stored as i18n messages

To add a new service, just add a new message which key's like `service.{serviceId}.label`. Please, keep in mind that first digit of `service id` is the `theme id` of the theme that the service belongs to.

## Updating themes

Themes are also stored as i18n messages

To add a new theme:

- add a new message which key's like `theme.{themeId}.label`
- add a new message which key's like `theme.{themeId}.description`

To make it appear in Theme filter page, add its themeId to `THEME_ORDER` list constant
defined in `src/constants.js`
