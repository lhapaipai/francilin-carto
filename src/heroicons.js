// From https://heroicons.dev/

export const chevronDown = {
	d: "M19 9l-7 7-7-7",
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const externalLink = {
	d: "M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14",
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const link = {
	d:
		"M13.828 10.172a4 4 0 00-5.656 0l-4 4a4 4 0 105.656 5.656l1.102-1.101m-.758-4.899a4 4 0 005.656 0l4-4a4 4 0 00-5.656-5.656l-1.1 1.1",
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const trash = {
	d:
		"M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z",
	viewBox: "0 0 24 24",
	variant: "solid",
}

export const close = {
	d: "M6 18L18 6M6 6l12 12",
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const translate = {
	d:
		"M3 5h12M9 3v2m1.048 9.5A18.022 18.022 0 016.412 9m6.088 9h7M11 21l5-10 5 10M12.751 5C11.783 10.77 8.07 15.61 3 18.129",
	viewBox: "0 0 24 24",
	variant: "outline",
}
