import { saveAs } from "file-saver"
import config from "~/config.js"

export async function saveUrlToPdf(url, filename, errorMessage) {
	const apiUrl = `https://chrome.browserless.io/pdf?token=${config.browserlessApiToken}`
	let response
	try {
		response = await fetch(apiUrl, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				url,
				options: {
					displayHeaderFooter: false,
					printBackground: false,
					format: "A4",
					margin: { top: "6mm", right: "6mm", bottom: "6mm", left: "6mm" },
				},
				gotoOptions: {
					waitUntil: "networkidle0",
				},
			}),
		})
	} catch (error) {
		alert(errorMessage)
		return
	}
	const blob = await response.blob()
	saveAs(blob, filename)
}
