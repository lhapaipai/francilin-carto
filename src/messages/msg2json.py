#!/usr/bin/env python3
import csv
import json
from collections import defaultdict


def update_dict(dict, path, value):
    if not "." in path:
        dict[path] = value if value != '' else None
    else:
        path_chunks = path.split(".")
        current_key = path_chunks[0]
        if current_key not in dict:
            dict[current_key] = {}
        update_dict(dict[current_key], '.'.join(path_chunks[1:]), value)


for lang in ('fr', 'en', 'ar'):

    lang_dict = {}
    with open("messages.csv", "rt", encoding="utf-8") as fd:
        reader = csv.DictReader(fd)

        for row in reader:
            update_dict(lang_dict, row["key"], row[lang])

    with open(f"{lang}.json", "wt", encoding="utf-8") as fd:
        json.dump(lang_dict, fd, indent=2, ensure_ascii=False, sort_keys=True)
