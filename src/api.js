import { normalizeProperties } from "./model"

import queryString from "query-string"
import config from "~/config.js"

async function fetchJSON(url) {
	const response = await fetch(url)
	return await response.json()
}

export async function fetchAddresses(text) {
	const url = queryString.stringifyUrl({
		url: config.banSearchUrl,
		query: { q: text, autocomplete: 1 },
	})
	// TODO validate data with a schema
	return await fetchJSON(url)
}

export async function fetchStructures() {
	// TODO validate data with a schema
	const data = await fetchJSON(config.structuresUrl)
	for (const feature of data.features) {
		feature.id = Number(feature.properties.francilin_id)
		feature.properties = normalizeProperties(feature.properties)
	}
	return data
}
