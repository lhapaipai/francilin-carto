import { isOpenNow, belongToTheme } from "./model"

export function filterFeatures(features, filters) {
	function satisfiesThemeFilter(feature, filterValue) {
		return (
			filterValue === null || feature.properties.services.some((serviceId) => belongToTheme(serviceId, filterValue))
		)
	}

	function satisfiesUrgenceFilter(feature, filterValue) {
		return !filterValue || isOpenNow(feature)
	}

	function satisfiesAideFilter(feature, filterValue) {
		return filterValue === null || feature.properties.aide.includes(filterValue)
	}

	const newFeatures = []

	for (const feature of features) {
		if (
			satisfiesThemeFilter(feature, filters.theme) &&
			satisfiesUrgenceFilter(feature, filters.urgence) &&
			satisfiesAideFilter(feature, filters.aide)
		) {
			newFeatures.push(feature)
		}
	}

	return newFeatures
}
