import { register, init } from "svelte-i18n"
import config from "./config.js"

export function initLocales() {
	register("ar", () => import("./messages/ar.json"))
	register("en", () => import("./messages/en.json"))
	register("fr", () => import("./messages/fr.json"))

	init({
		fallbackLocale: config.defaultLanguage,
		initialLocale: config.defaultLanguage,
	})
}
