import alias from "@rollup/plugin-alias"
import dotenv from "rollup-plugin-dotenv"
import json from "@rollup/plugin-json"
import postcss from "rollup-plugin-postcss"
import replace from "@rollup/plugin-replace"
import sveltePreprocess from "svelte-preprocess"
import { createRollupConfigs } from "./scripts/base.config.js"

const production = !process.env.ROLLUP_WATCH

const onwarn = (warning, onwarn) => {
	// ignores the annoying this is undefined warning
	// Cf https://github.com/kaisermann/svelte-i18n/blob/master/docs/FAQ.md#this-keyword-is-equivalent-to-undefined
	if (warning.code === "THIS_IS_UNDEFINED") {
		return
	}

	onwarn(warning)
}

export const config = {
	staticDir: "static",
	distDir: "dist",
	buildDir: `dist/build`,
	serve: !production,
	production,
	rollupWrapper: (rollup) => {
		rollup.onwarn = onwarn
		rollup.plugins = [
			...rollup.plugins,
			alias({ entries: [{ find: "~", replacement: __dirname + "/src" }] }),
			dotenv(),
			replace({
				// Used by @popperjs/core
				"process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV),
			}),
			json(),
			postcss(),
		]
	},
	svelteWrapper: (svelte) => {
		svelte.preprocess = [sveltePreprocess({ postcss: true })]
	},
}

const configs = createRollupConfigs(config)

export default configs

/**
  Wrappers can either mutate or return a config

  wrapper example 1
  svelteWrapper: (cfg, ctx) => {
    cfg.preprocess: mdsvex({ extension: '.md' }),
  }

  wrapper example 2
  rollupWrapper: cfg => {
    cfg.plugins = [...cfg.plugins, myPlugin()]
    return cfg
  }
*/
